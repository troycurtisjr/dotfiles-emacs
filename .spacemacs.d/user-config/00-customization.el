;; Various small bits of spacemacs and layer customizations

(add-to-list 'exec-path "/usr/local/bin")'
(add-to-list 'exec-path "/Users/troycurtisjr/.node/node_modules/tern/bin")'

(setq-default vinegar-reuse-dired-buffer t)

(setq-default evil-escape-key-sequence "kj")
;; prevent esc-key from translating to meta-key in terminal mode, but doesn't really seems to work.
(setq-default evil-esc-delay 0)


(spacemacs/set-leader-keys "fF" 'helm-find)

(spacemacs/set-leader-keys "so" 'helm-occur)

;; Do not use the periodic autosave for recentf. When a laptop wakes up from
;; sleep, all emacs processes show "expired" timer and try to simultaneously
;; lock and save to recentf. This timer disabling still allows gracefully
;; exiting processes to write to the recentf however, so it isn't a complete
;; loss of functionality.
(cancel-timer recentf-auto-save-timer)
