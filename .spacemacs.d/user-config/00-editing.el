;; General configuration related to editing

(setq-default c-basic-offset 2
              tab-width 2
              fill-column 100)

(setq-default c-default-style
              '((java-mode . "java")
                (awk-mode . "awk")
                (other . "bsd")))

(setq-default company-shell-dont-fetch-meta t)
