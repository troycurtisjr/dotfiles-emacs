;; Configuration for lsp-mode

(setq lsp-ui-sideline-show-code-actions nil)
(setq lsp-ui-doc-enable nil)
