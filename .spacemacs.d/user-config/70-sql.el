;; SQL Mode Configuration
;; See ~/.pgpass for passwords
(require 'sql-indent)

;; To be filled out by local configuration
(setq sql-connection-alist '())
(setq sql-postgres-program "/usr/local/Cellar/postgresql@13/13.8/bin/psql")

(add-to-list 'exec-path "/usr/local/opt/postgresql@10/bin")

(add-hook 'sql-mode-hook
          (lambda ()
            ;; Prompt for the port if not given
            (setq sql-postgres-login-params (append sql-postgres-login-params '(port)))
            (sql-set-product-feature 'postgres :prompt-regexp "^[-[:alnum:]_]*=[#>] ")
            (sql-set-product-feature 'postgres :prompt-cont-regexp
                                     "^[-[:alnum:]_]*[-(][#>] ")
            (setq-local ac-ignore-case t)
            (auto-complete-mode)))

(add-hook 'sql-interactive-mode-hook
          (lambda ()
            ;; Don't allow long lines to wrap
            (toggle-truncate-lines t)
            (setq-local show-trailing-whitespace nil)
            (auto-complete-mode t)))

(add-hook 'sql-login-hook 'my-sql-login-hook)

(defun my-sql-login-hook ()
  "Custom SQL log-in behaviours. See `sql-login-hook'."
  ;; n.b. If you are looking for a response and need to parse the
  ;; response, use `sql-redirect-value' instead of `comint-send-string'.
  (when (eq sql-product 'postgres)
    (let ((proc (get-buffer-process (current-buffer))))
      ;; Output each query before executing it. (n.b. this also avoids
      ;; the psql prompt breaking the alignment of query results.)
      (comint-send-string proc "\\set ECHO queries\n")
      ;; Try to keep the memory usage down if I accidentally try to fetch a very
      ;; large dataset
      (comint-send-string proc "\\set FETCH_COUNT 1000\n")
      (comint-send-string proc "\\timing\n")
      (comint-send-string proc "\\set PROMPT1 '%n-%m-%/%R%# '\n")
      (comint-send-string proc "\\set PROMPT2 '%n-%m-%/%R%# '\n")
      )))

;; Helm based selection for selection a connection from the list above
(defun my-sql-connect (connection)
  (interactive
   (helm-comp-read "Select Server: " (mapcar (lambda (item)
                                               (list
                                                (symbol-name (nth 0 item))
                                                (nth 0 item)))
                                             sql-connection-alist)))
  (setq connection-info (assoc connection sql-connection-alist))
  (setq sql-product (nth 1 (nth 1 (assoc 'sql-product connection-info))))
  ;; connect to database
  (sql-connect connection connection)
  ;; (if current-prefix-arg
  ;;     (sql-connect connection connection)
  ;;   (sql-connect connection)))
  )

(spacemacs/set-leader-keys-for-major-mode 'sql-mode "sc" 'my-sql-connect)
(defvar my-sql-indentation-offsets-alist
  `((select-clause 0)
    (insert-clause 0)
    (delete-clause 0)
    (update-clause 0)
    ,@sqlind-default-indentation-offsets-alist))

(add-hook 'sqlind-minor-mode-hook
          (lambda ()
            (setq sqlind-indentation-offsets-alist
                  my-sql-indentation-offsets-alist)))
