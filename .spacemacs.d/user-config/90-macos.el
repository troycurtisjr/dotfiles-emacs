;; Configuration to work better with mac

;; Macos ls doesn't have --dired mode
(when (string= system-type "darwin")
  (setq dired-use-ls-dired nil))
