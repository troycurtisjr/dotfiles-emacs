(require 'ejc-sql)

(setq clomacs-httpd-default-port 8090)

;; (require 'ejc-autocomplete)
;; (add-hook 'ejc-sql-minor-mode-hook
;;           (lambda ()
;;             (auto-complete-mode t)
;;             (ejc-ac-setup)))

(add-hook 'ejc-sql-connected-hook
          (lambda ()
            (ejc-set-fetch-size 200)
            (ejc-set-max-rows 200)
            (ejc-set-show-too-many-rows-message t)
            (ejc-set-column-width-limit 300)
            (ejc-set-use-unicode t)))

;; (setq ejc-result-table-impl 'orgtbl-mode)
;; Supposedly has some performance benefit, but maybe not as much functionality
;; Mostly I think it is the "spreadsheet" functionality from orgtbl
(setq ejc-result-table-impl 'ejc-result-mode)

