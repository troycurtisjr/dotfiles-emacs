;; I'm not currently actually using this, but saved for posterity

;; Include the org-jira layer to use this

(setq jiralib-url "https://bitsight.atlassian.net")
(setq org-jira-working-dir "~/Notes/org/jira")
(setq org-jira-custom-jqls
      '(
        (:jql " status in ('In Progress', 'Pending Feedback', 'Pending Test', 'Waiting for Release') AND resolution = Unresolved AND assignee in (currentUser()) ORDER BY updated DESC"
              :filename "my-inflight"
              )
        ))
