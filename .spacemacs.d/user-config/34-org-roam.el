;; (set org-roam-directory "~/Notes/roam")
(global-set-key (kbd "C-c n l") 'org-roam-buffer-toggle)
(global-set-key (kbd "C-c n f") 'org-roam-node-find)
(global-set-key (kbd "C-c n i") 'org-roam-node-insert)
(global-set-key (kbd "C-c n d n") 'org-roam-dailies-capture-today)
(global-set-key (kbd "C-c n d Y") 'org-roam-dailies-capture-yesterday)
(global-set-key (kbd "C-c n d T") 'org-roam-dailies-capture-tomorrow)
(global-set-key (kbd "C-c n d y") 'org-roam-dailies-goto-yesterday)
(global-set-key (kbd "C-c n d t") 'org-roam-dailies-goto-tomorrow)
(global-set-key (kbd "C-c n d d") 'org-roam-dailies-goto-today)
(global-set-key (kbd "C-c n d v") 'org-roam-dailies-capture-date)
(global-set-key (kbd "C-c n d c") 'org-roam-dailies-goto-date)
(global-set-key (kbd "C-c n d b") 'org-roam-dailies-goto-next-note)
(global-set-key (kbd "C-c n d f") 'org-roam-dailies-goto-previous-note)

(setq org-roam-dailies-capture-templates
      '(("d" "default" entry "* %?\n%U"
         :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))
