;; magit customizations

(setq-default git-enable-magit-svn-plugin t)

;; Allows emacs to be be used easily as the EDITOR
;;(global-git-commit-mode t)
