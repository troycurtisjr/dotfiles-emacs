;; Org specific configurations
;;
;; Many of these settings originate from http://doc.norang.ca/org-mode.html#Capture
;; This file is meant to be sourced immediately.
;;

;; org-journal configuration
(setq-default org-journal-dir "~/Notes/journal/")
(setq-default org-journal-date-prefix "#+TITLE: ")
(setq-default org-journal-time-prefix "* ")

;; This is necessary because the spacemacs org layer is overwriting some values,
;; most notably org-default-notes-file.
;; Fix was merged Oct 2017, but still not released as of 2019...
;; https://github.com/syl20bnr/spacemacs/issues/9748
(with-eval-after-load 'org
  (load "~/.spacemacs.d/user-config/35-agenda-setup")
  (org-defkey org-mode-map [(meta return)] 'org-meta-return)
  (load "~/.spacemacs.d/user-config/33-org-skeletons")
  )

;; To get <s<TAB> working again.
(when (version<= "9.2" (org-version))
  (require 'org-tempo))

(add-to-list 'auto-mode-alist '("\\.\\(org\\|org_archive\\)$" . org-mode))

;; Custom Key Bindings from norang, some I've commented out
(global-set-key (kbd "<f12>") 'org-agenda)
(global-set-key (kbd "<f5>") 'bh/org-todo)
(global-set-key (kbd "<S-f5>") 'bh/widen)
(global-set-key (kbd "<f7>") 'bh/set-truncate-lines)
(global-set-key (kbd "<f8>") 'org-cycle-agenda-files)
(global-set-key (kbd "<f9> <f9>") 'bh/show-org-agenda)
(global-set-key (kbd "<f9> b") 'bbdb)
(global-set-key (kbd "<f9> c") 'calendar)
(global-set-key (kbd "<f9> f") 'boxquote-insert-file)
(global-set-key (kbd "<f9> g") 'gnus)
(global-set-key (kbd "<f9> h") 'bh/hide-other)
(global-set-key (kbd "<f9> n") 'bh/toggle-next-task-display)

(global-set-key (kbd "<f9> I") 'bh/punch-in)
(global-set-key (kbd "<f9> O") 'bh/punch-out)

(global-set-key (kbd "<f9> o") 'bh/make-org-scratch)

(global-set-key (kbd "<f9> r") 'boxquote-region)
(global-set-key (kbd "<f9> s") 'bh/switch-to-scratch)

(global-set-key (kbd "<f9> t") 'bh/insert-inactive-timestamp)
(global-set-key (kbd "<f9> T") 'bh/toggle-insert-inactive-timestamp)

(global-set-key (kbd "<f9> v") 'visible-mode)
(global-set-key (kbd "<f9> l") 'org-toggle-link-display)
(global-set-key (kbd "<f9> SPC") 'bh/clock-in-last-task)
(global-set-key (kbd "C-<f9>") 'previous-buffer)
(global-set-key (kbd "M-<f9>") 'org-toggle-inline-images)
;; (global-set-key (kbd "C-x n r") 'narrow-to-region)
(global-set-key (kbd "C-<f10>") 'next-buffer)
(global-set-key (kbd "<f11>") 'org-clock-goto)
(global-set-key (kbd "C-<f11>") 'org-clock-in)
;; (global-set-key (kbd "C-c c") 'org-capture)

;; (org-babel-do-load-languages
;;  'org-babel-load-languages
;;  '((dot . t))) ; this line activates dot
